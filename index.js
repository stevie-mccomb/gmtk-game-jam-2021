const express = require('express');
const http = require('http');

const app = express();
const server = http.createServer(app);

app.use(express.static('public'));

const port = process.env.PORT ? process.env.PORT : 3000;
server.listen(port, () => {
    console.log(`Listening on *:${port}...`);
});
