import ControllerListener from 'Interfaces/ControllerListener';
import MouseData from 'Interfaces/MouseData';
import Stage from 'Classes/Stage';
import Vector from 'Classes/Abstracts/Vector';

export default class Controller
{
    private static _instance: Controller;

    private keys: string[] = [];
    private mouse: MouseData = {
        buttonDown: false,
        position: {
            x: 0,
            y: 0
        }
    };

    private listeners: ControllerListener[] = [];

    private onKeydownBound: EventListener = this.onKeydown.bind(this);
    private onKeyupBound: EventListener = this.onKeyup.bind(this);
    private onMousedownBound: EventListener = this.onMousedown.bind(this);
    private onMouseupBound: EventListener = this.onMouseup.bind(this);
    private onMousemoveBound: EventListener = this.onMousemove.bind(this);

    constructor()
    {
        Controller._instance = this;

        document.addEventListener('keydown', this.onKeydownBound);
        document.addEventListener('keyup', this.onKeyupBound);
        Stage.instance.element.addEventListener('mousedown', this.onMousedownBound);
        Stage.instance.element.addEventListener('mouseup', this.onMouseupBound);
        Stage.instance.element.addEventListener('mousemove', this.onMousemoveBound);
    }

    public on(event: string, callback: EventListener): void
    {
        this.listeners.push({ event, callback });
    }

    public off(event: string, callback?: EventListener): void
    {
        for (let i = this.listeners.length - 1; i >= 0; --i) {
            const listener = this.listeners[i];

            if (listener.event !== event) continue;
            if (!!callback && listener.callback !== callback) continue;
            this.listeners.splice(i, 1);
        }
    }

    private onKeydown(e: KeyboardEvent): void
    {
        const key = e.key.toLowerCase();
        const index = this.keys.indexOf(key);

        if (index < 0) this.keys.push(key);

        for (const listener of this.listeners) {
            if (listener.event === 'keydown') listener.callback(e);
        }
    }

    private onKeyup(e: KeyboardEvent): void
    {
        const key = e.key.toLowerCase();
        const index = this.keys.indexOf(key);

        if (index >= 0) this.keys.splice(index, 1);

        for (const listener of this.listeners) {
            if (listener.event === 'keyup') listener.callback(e);
        }
    }

    private onMousedown(e: MouseEvent): void
    {
        this.mouse.buttonDown = true;
    }

    private onMouseup(e: MouseEvent): void
    {
        this.mouse.buttonDown = false;
    }

    private onMousemove(e: MouseEvent): void
    {
        this.mouse.position.x = e.offsetX;
        this.mouse.position.y = e.offsetY;
    }

    public isDown(key: string): boolean
    {
        return this.keys.indexOf(key.toLowerCase()) >= 0;
    }

    public isUp(key: string): boolean
    {
        return !this.isDown(key);
    }

    public mouseIsDown(): boolean
    {
        return this.mouse.buttonDown;
    }

    public mouseIsUp(): boolean
    {
        return !this.mouse.buttonDown;
    }

    public mousePosition(): Vector
    {
        return new Vector(this.mouse.position.x, this.mouse.position.y);
    }

    public static get instance(): Controller
    {
        return Controller._instance;
    }
}
