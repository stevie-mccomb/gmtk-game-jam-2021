import Enemy from 'Classes/GameObjects/Enemy';

export default class EnemySpawner
{
    private static _instance: EnemySpawner;

    private maxEnemies: number = 30;
    private minTimeBetweenEnemies: number = 3000;
    private maxTimeBetweenEnemies: number = 10000;
    private timeout: number = 0;
    private spawnBound: EventListener = this.spawn.bind(this);

    constructor()
    {
        EnemySpawner._instance = this;

        this.spawn();
    }

    private refreshSpawnTimeout(): void
    {
        const delay = Math.floor(Math.random() * (this.maxTimeBetweenEnemies - this.minTimeBetweenEnemies)) + this.minTimeBetweenEnemies;
        if (this.timeout) clearTimeout(this.timeout);
        this.timeout = Number(setTimeout(this.spawnBound, delay));
    }

    private spawn(): void
    {
        if (Enemy.instances.length < this.maxEnemies) new Enemy();
        this.refreshSpawnTimeout();
    }

    public destroy(): void
    {
        if (this.timeout) clearTimeout(this.timeout);
        EnemySpawner._instance = undefined;
    }

    public static get instance(): EnemySpawner
    {
        return EnemySpawner._instance;
    }
};
