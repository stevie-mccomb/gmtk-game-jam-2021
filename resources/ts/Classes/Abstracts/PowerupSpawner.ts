import ExtraLife from 'Classes/GameObjects/Powerups/ExtraLife';
import Invulnerability from 'Classes/GameObjects/Powerups/Invulnerability';
import Powerup from 'Classes/GameObjects/Powerups/Powerup';
import RapidFire from 'Classes/GameObjects/Powerups/RapidFire';
import SpreadFire from 'Classes/GameObjects/Powerups/SpreadFire';
import Stage from 'Classes/Stage';

export default class PowerupSpawner
{
    private minSpawnTime: number = 30000;
    private maxSpawnTime: number = 60000;
    private timeout: number;
    private spawnBound: EventListener = this.spawn.bind(this);

    private powerups: any[] = [
        ExtraLife,
        Invulnerability,
        RapidFire,
        SpreadFire,
    ];

    constructor()
    {
        this.spawn();
    }

    spawn(): void
    {
        const position = {
            x: Math.randomFloatBetween(Stage.instance.left, Stage.instance.right),
            y: Math.randomFloatBetween(Stage.instance.top, Stage.instance.bottom)
        };

        const randomPowerup = this.powerups.random();
        const powerup = new randomPowerup();
        powerup.center = position;

        if (this.timeout) clearTimeout(this.timeout);
        this.timeout = Number(setTimeout(this.spawnBound, Math.randomIntBetween(this.minSpawnTime, this.maxSpawnTime)));
    }
}
