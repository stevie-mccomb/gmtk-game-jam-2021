import { Animation, Config, Frame } from 'Interfaces/SpriteConfig';
import GameObject from 'Classes/GameObjects/GameObject';
import Stage from 'Classes/Stage';

export default class Sprite
{
    private owner: GameObject;
    private config: Config;
    private image: HTMLImageElement = new Image();
    private frameIndex: number = 0;
    private frameTicks: number = 0;
    private _animationName: string = 'idle';
    private pattern: CanvasPattern;
    private onImageLoadBound: EventListener = this.onImageLoad.bind(this);

    constructor(owner: GameObject, config: Config)
    {
        this.owner = owner;
        this.config = config;
        this.animationName = config.default_animation || Object.keys(this.config.animations)[0] || 'idle';

        this.image.onload = this.onImageLoadBound;
        this.image.src = this.config.src;
    }

    public update(): void
    {
        ++this.frameTicks;

        if (this.frameTicks >= this.frameRate) {
            this.frameTicks = 0;
            const shouldLoop = (this.animation.loop === true || typeof this.animation.loop === 'undefined');
            this.frameIndex = this.animation.frames[this.frameIndex + 1] ? this.frameIndex + 1 : (shouldLoop ? 0 : this.frameIndex);
        }
    }

    public render(): void
    {
        if (this.animation && this.animation.tiles && this.pattern) {
            Stage.instance.context.fillStyle = this.pattern;
            Stage.instance.context.fillRect(this.owner.x, this.owner.y, this.owner.width, this.owner.height);
        } else {
            Stage.instance.context.drawImage(
                this.image,
                this.frame.x,
                this.frame.y,
                this.width,
                this.height,
                this.owner.x,
                this.owner.y,
                this.width,
                this.height
            );
        }
    }

    private onImageLoad(e: ProgressEvent)
    {
        if (this.animation.tiles) {
            this.pattern = Stage.instance.context.createPattern(this.image, 'repeat');
        }
    }

    public get width(): number
    {
        if (this.frame && this.frame.width) return this.frame.width;
        if (this.animation && this.animation.width) return this.animation.width;
        if (this.config && this.config.width) return this.config.width;
        if (this.owner && this.owner.width) return this.owner.width;
        return 0;
    }

    public get height(): number
    {
        if (this.frame && this.frame.height) return this.frame.height;
        if (this.animation && this.animation.height) return this.animation.height;
        if (this.config && this.config.height) return this.config.height;
        if (this.owner && this.owner.height) return this.owner.height;
        return 0;
    }

    public get animation(): Animation
    {
        const animation = this.config.animations[this._animationName];
        if (!animation) return this.config.animations[this.config.default_animation];
        if (!animation) return null;
        return animation;
    }

    public get animationName(): string
    {
        return this._animationName;
    }

    public set animationName(value: string)
    {
        if (this.animationName !== value) {
            this.frameIndex = 0;
            if (!this.config.animations[value]) throw new Error(`Sprite has no animation named "${value}".`);
            this._animationName = value;
        }
    }

    public get frame(): Frame
    {
        if (!this.animation) return null;
        const frame = this.animation.frames[this.frameIndex];
        if (!frame) throw new Error(`Sprite attempted to render out-of-bounds frame. Animation named "${this.animationName}" has no frame at index ${this.frameIndex}.`);
        return frame;
    }

    public get frameRate(): number
    {
        if (this.animation && this.animation.frame_rate) return this.animation.frame_rate;
        if (this.config && this.config.frame_rate) return this.config.frame_rate;
        return 60;
    }
}
