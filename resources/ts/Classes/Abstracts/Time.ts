export default class Time
{
    public static now: number = 0;
    public static delta: number = 0;

    public static get deltaMilliseconds(): number
    {
        return Time.delta;
    }

    public static get deltaSeconds(): number
    {
        return Time.delta / 1000;
    }
}
