import Wall from 'Classes/GameObjects/Wall';

export default class WallSpawner
{
    private minSpawnTime: number = 1000;
    private maxSpawnTime: number = 5000;
    private timeout: number;
    private spawnBound: EventListener = this.spawn.bind(this);

    constructor()
    {
        this.spawn();
    }

    spawn(): void
    {
        new Wall();

        if (this.timeout) clearTimeout(this.timeout);
        this.timeout = Number(setTimeout(this.spawnBound, Math.randomIntBetween(this.minSpawnTime, this.maxSpawnTime)));
    }
}
