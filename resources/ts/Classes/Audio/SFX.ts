export default class SFX
{
    private audio: HTMLAudioElement = new Audio();
    private onLoadBound: EventListener = this.onLoad.bind(this);
    private queued: boolean = false;
    public fade: boolean = false;
    private prefadeVolume: number = 1.0;

    constructor(src: string, queued: boolean = false)
    {
        this.audio.addEventListener('canplay', this.onLoadBound);

        this.audio.src = src;
        this.queued = queued;
    }

    private onLoad(e: ProgressEvent): void
    {
        if (this.queued) {
            this.play();
            this.queued = false;
        }
    }

    public play(): void
    {
        if (this.loaded) {
            if (this.fade) {
                this.prefadeVolume = this.volume;
                this.volume = 0.0;
                this.audio.play();
                const interval = setInterval(() => {
                    this.volume += 0.05;
                    if (this.volume >= this.prefadeVolume) {
                        clearInterval(interval);
                        this.volume = this.prefadeVolume;
                    }
                }, 50);
            } else {
                this.audio.play();
            }
        } else {
            this.queued = true;
        }
    }

    public pause(): void
    {
        if (this.fade) {
            this.prefadeVolume = this.volume;
            const interval = setInterval(() => {
                this.volume -= 0.05;
                if (this.volume <= 0) {
                    clearInterval(interval);
                    this.audio.pause();
                    this.volume = this.prefadeVolume;
                }
            }, 50);
        } else {
            this.audio.pause();
        }
    }

    public get currentTime(): number
    {
        return this.audio.currentTime;
    }

    public set currentTime(value: number)
    {
        this.audio.currentTime = value.clamp(0.0, this.audio.duration);
    }

    public get loaded(): boolean
    {
        return [2, 3, 4].indexOf(this.audio.readyState) >= 0;
    }

    public get volume(): number
    {
        return this.audio.volume;
    }

    public set volume(value: number)
    {
        this.audio.volume = value.clamp(0.0, 1.0);
    }
}
