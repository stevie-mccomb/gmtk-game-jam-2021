import Controller from 'Classes/Abstracts/Controller';
import GameObject from 'Classes/GameObjects/GameObject';
import MasterMachine from 'Classes/Machines/MasterMachine';
import Stage from 'Classes/Stage';
import Time from 'Classes/Abstracts/Time';

export default class Game
{
    private static _instance: Game;
    private _element: HTMLDivElement;
    private loopBound: FrameRequestCallback = this.loop.bind(this);
    private animationFrame: number = 0;
    private lastUpdated: number = 0;
    public paused: boolean = false;

    constructor()
    {
        Game._instance = this;

        this._element = document.createElement('div');
        this._element.className = 'game';
        document.body.appendChild(this._element);

        new Stage();
        new Controller();
        new MasterMachine();

        this.animationFrame = requestAnimationFrame(this.loopBound);
    }

    private loop(timestamp: number): void
    {
        Time.now = timestamp;
        Time.delta = timestamp - this.lastUpdated;

        if (!this.paused) {
            this.update();
            this.render();
        }

        this.lastUpdated = timestamp;

        this.animationFrame = requestAnimationFrame(this.loopBound);
    }

    private update(): void
    {
        Stage.instance.update();
        MasterMachine.instance.update();
    }

    private render(): void
    {
        Stage.instance.render();
        MasterMachine.instance.render();
    }

    public static get instance(): Game
    {
        return Game._instance;
    }

    public get element(): HTMLDivElement
    {
        return this._element;
    }
}
