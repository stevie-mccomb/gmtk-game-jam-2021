import GameObject from 'Classes/GameObjects/GameObject';
import Hero from 'Classes/GameObjects/Hero';
// import Sprite from 'Classes/Abstracts/Sprite';
// import SpriteConfig from 'Sprites/EnemySprite';
import Stage from 'Classes/Stage';
import Time from 'Classes/Abstracts/Time';
import Vector from 'Classes/Abstracts/Vector';

export default class Enemy extends GameObject
{
    protected static _instances: Enemy[] = [];

    public width: number = 32;
    public height: number = 32;
    protected speed: number = 180;
    protected facing: string = 'down';
    // public sprite: Sprite = new Sprite(this, SpriteConfig);
    public health: number = 0;
    public maxHealth: number = 3;

    constructor(x: number = 0, y: number = 0)
    {
        super(x, y);

        Enemy._instances.push(this);

        this.collidable = true;
        this.immovable = true;
        this.health = this.maxHealth;

        while ((this.x === 0 && this.y === 0) || this.colliding(Hero.instance)) {
            this.randomizePosition();
        }
    }

    public update(): void
    {
        this.move();
        this.checkCollisions();
    }

    public render(): void
    {
        Stage.instance.context.fillStyle = 'red';
        Stage.instance.context.beginPath();
        Stage.instance.context.arc(this.center.x, this.center.y, this.width / 2, 0, Math.PI * 2);
        Stage.instance.context.fill();

        const healthBarOffset = 4;
        const healthBarHeight = 8;
        const healthBarCapSize = 4;
        const healthBarWidth = this.width - (healthBarCapSize * 2);
        const healthBarPosition = {
            x: this.x + healthBarCapSize,
            y: this.y - healthBarHeight - healthBarOffset
        };

        Stage.instance.context.fillStyle = 'darkred';
        Stage.instance.context.fillRect(healthBarPosition.x, healthBarPosition.y, healthBarWidth, healthBarHeight);
        Stage.instance.context.beginPath();
        Stage.instance.context.arc(healthBarPosition.x, healthBarPosition.y + (healthBarHeight / 2), healthBarCapSize, 0, Math.PI * 2);
        Stage.instance.context.arc(healthBarPosition.x + healthBarWidth, healthBarPosition.y + (healthBarHeight / 2), healthBarCapSize, 0, Math.PI * 2);
        Stage.instance.context.fill();

        const healthBarBorderSize = 2;
        Stage.instance.context.fillStyle = 'red';
        Stage.instance.context.fillRect(
            healthBarPosition.x + healthBarBorderSize,
            healthBarPosition.y + healthBarBorderSize,
            (healthBarWidth * (this.health / this.maxHealth)) - (healthBarBorderSize * 2),
            healthBarHeight - (healthBarBorderSize * 2)
        );
        Stage.instance.context.beginPath();
        Stage.instance.context.arc(healthBarPosition.x + healthBarBorderSize, healthBarPosition.y + (healthBarHeight / 2), healthBarCapSize - healthBarBorderSize, 0, Math.PI * 2);
        if (this.health === this.maxHealth) {
            Stage.instance.context.arc(healthBarPosition.x + healthBarWidth - healthBarBorderSize, healthBarPosition.y + (healthBarHeight / 2), healthBarCapSize - healthBarBorderSize, 0, Math.PI * 2);
        }
        Stage.instance.context.fill();
    }

    public destroy(): void
    {
        super.destroy();

        const index = Enemy._instances.indexOf(this);
        if (index >= 0) Enemy._instances.splice(index, 1);
    }

    private randomizePosition(): void
    {
        this.x = Math.floor(Math.random() * (Stage.instance.right - this.width));
        this.y = Math.floor(Math.random() * (Stage.instance.bottom - this.height));
    }

    private move(): void
    {
        const target = Hero.instance.dead ? Stage.instance.center : Hero.instance.center;
        const distanceX = Hero.instance.dead ? (this.x - target.x) : (target.x - this.x);
        const distanceY = Hero.instance.dead ? (this.y - target.y) : (target.y - this.y);

        const angle = Math.atan2(distanceY, distanceX);
        const velocity = new Vector(Math.cos(angle), Math.sin(angle));

        velocity.normalize();

        this.x += (velocity.x * this.speed * Time.deltaSeconds);
        this.y += (velocity.y * this.speed * Time.deltaSeconds);

        const absoluteVelocity = new Vector(Math.abs(velocity.x), Math.abs(velocity.y));

        switch (true) {
            case (velocity.x < 0 && absoluteVelocity.x > absoluteVelocity.y):
                this.facing = 'left';
                break;

            case (velocity.x > 0 && absoluteVelocity.x > absoluteVelocity.y):
                this.facing = 'right';
                break;

            case (velocity.y < 0 && absoluteVelocity.y > absoluteVelocity.x):
                this.facing = 'up';
                break;

            case (velocity.y > 0 && absoluteVelocity.y > absoluteVelocity.x):
            default:
                this.facing = 'down';
                break;
        }

        // this.sprite.animationName = `walk_${this.facing}`;
    }

    private checkCollisions(): void
    {
        for (const otherEnemy of Enemy.instances) {
            if (otherEnemy === this) continue;
            this.bumpAgainst(otherEnemy);
        }

        if (this.colliding(Hero.instance)) {
            Hero.instance.die();
        }
    }

    public damage(damage: number): void
    {
        this.health -= damage;
        if (this.health <= 0) this.destroy();
    }

    public static get instances(): Enemy[]
    {
        return Enemy._instances;
    }
}
