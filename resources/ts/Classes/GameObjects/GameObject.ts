import Sprite from 'Classes/Abstracts/Sprite';
import Stage from 'Classes/Stage';
import Vector from 'Interfaces/Vector';

export default class GameObject
{
    protected static _instances: GameObject[] = [];
    protected static _collidables: GameObject[] = [];

    public x: number = 0;
    public y: number = 0;
    public width: number = 0;
    public height: number = 0;
    protected _collidable: boolean = false;
    protected immovable: boolean = false;
    public sprite: Sprite;

    constructor(x: number = 0, y: number = 0, width: number = 0, height: number = 0)
    {
        GameObject._instances.push(this);

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public update(): void
    {
        //
    }

    public render(): void
    {
        if (this.sprite) {
            this.sprite.update();
            this.sprite.render();
        }
    }

    public destroy(): void
    {
        const index = GameObject._instances.indexOf(this);
        if (index >= 0) GameObject._instances.splice(index, 1);
    }

    public colliding(other: GameObject): boolean|string
    {
        const isColliding = (
            this.left < other.right
               &&
            this.right > other.left
               &&
            this.top < other.bottom
               &&
            this.bottom > other.top
        );

        if (isColliding) {
            const diffTop = Math.abs(this.top - other.bottom);
            const diffLeft = Math.abs(this.left - other.right);
            const diffBottom = Math.abs(this.bottom - other.top);
            const diffRight = Math.abs(this.right - other.left);

            const shallowest = Math.min(diffTop, diffLeft, diffBottom, diffRight);

            if (shallowest === diffTop) return 'top';
            if (shallowest === diffLeft) return 'left';
            if (shallowest === diffBottom) return 'bottom';
            if (shallowest === diffRight) return 'right';
        }

        return isColliding;
    }

    protected bumpAgainst(other: GameObject, reverse: boolean = false)
    {
        const side = this.colliding(other);

        switch (side) {
            case 'top':
                reverse ? (other.bottom = this.top) : (this.top = other.bottom);
                break;

            case 'left':
                reverse ? (other.right = this.left) : (this.left = other.right);
                break;

            case 'bottom':
                reverse ? (other.top = this.bottom) : (this.bottom = other.top);
                break;

            case 'right':
                reverse ? (other.left = this.right) : (this.right = other.left);
                break;

            default:
                break;
        }
    }

    public static get instances(): GameObject[]
    {
        return GameObject._instances;
    }

    public static get collidables(): GameObject[]
    {
        return GameObject._collidables;
    }

    public get center(): Vector
    {
        return {
            x: this.x + (this.width / 2),
            y: this.y + (this.height / 2)
        };
    }

    public get top(): number
    {
        return this.y;
    }

    public get left(): number
    {
        return this.x;
    }

    public get bottom(): number
    {
        return this.y + this.height;
    }

    public get right(): number
    {
        return this.x + this.width;
    }

    public get collidable(): boolean
    {
        return this._collidable;
    }

    public get offStage(): boolean
    {
        return (
            this.top > Stage.instance.bottom
                ||
            this.left > Stage.instance.right
                ||
            this.bottom < Stage.instance.top
                ||
            this.right < Stage.instance.left
        );
    }

    public set center(value: Vector)
    {
        this.x = value.x - (this.width / 2);
        this.y = value.y - (this.height / 2);
    }

    public set top(value: number)
    {
        this.y = value;
    }

    public set left(value: number)
    {
        this.x = value;
    }

    public set bottom(value: number)
    {
        this.y = value - this.height;
    }

    public set right(value: number)
    {
        this.x = value - this.width;
    }

    public set collidable(value: boolean)
    {
        if (value !== this._collidable) {
            this._collidable = value;

            if (this._collidable) {
                GameObject._collidables.push(this);
            } else {
                const index = GameObject._collidables.indexOf(this);
                if (index >= 0) GameObject._collidables.splice(index, 1);
            }
        }
    }
}
