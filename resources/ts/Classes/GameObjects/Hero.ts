import AliveState from 'Classes/States/Hero/AliveState';
import Controller from 'Classes/Abstracts/Controller';
import DeadState from 'Classes/States/Hero/DeadState';
import Game from 'Classes/Game';
import GameObject from 'Classes/GameObjects/GameObject';
import MasterMachine from 'Classes/Machines/MasterMachine';
import PlayState from 'Classes/States/PlayState';
import Projectile from 'Classes/GameObjects/Projectile';
import RespawningState from 'Classes/States/Hero/RespawningState';
import SFX from 'Classes/Audio/SFX';
import Sprite from 'Classes/Abstracts/Sprite';
import SpriteConfig from 'Sprites/HeroSprite';
import Stage from 'Classes/Stage';
import StateMachine from 'Classes/Machines/StateMachine';
import Time from 'Classes/Abstracts/Time';
import Vector from 'Classes/Abstracts/Vector';

export default class Hero extends GameObject
{
    private static _instance: Hero;

    public width: number = 32;
    public height: number = 32;
    protected speed: number = 240;
    protected facing: string = 'down';
    protected moving: boolean = false;

    private _lives: number = 3;
    private respawnTime: number = 2000;
    private lifeTracker: HTMLDivElement = document.createElement('div');
    public invulnerable: boolean = false;

    public fireRate: number = 16; // number of frames between bullets
    private canFire: boolean = true;
    private framesSinceLastFire: number = 0;
    public hasSpread: boolean = false;

    private overheated: boolean = false;
    private currentHeat: number = 0;
    private maxHeat: number = 1;
    private framesUntilCooling: number = 16;
    private framesSinceCooling: number = 0;
    private framesSinceLastFireAttempt: number = 0;
    private coolingRate: number = 0.005;

    public stateMachine: StateMachine = new StateMachine();

    public sprite: Sprite = new Sprite(this, SpriteConfig);
    // private deathAudio: SFX = new SFX('/audio/sfx/hero-death.wav');

    constructor()
    {
        super();

        Hero._instance = this;

        this.collidable = true;
        this.center = Stage.instance.center;
        this.lives = 3;

        this.lifeTracker.className = 'life-tracker';
        Game.instance.element.appendChild(this.lifeTracker);

        this.stateMachine.register('alive', new AliveState());
        this.stateMachine.register('dead', new DeadState());
        this.stateMachine.register('respawning', new RespawningState());
        this.stateMachine.change('alive');
    }

    public update(): void
    {
        if (this.dead || this.respawning) return;

        this.move();
        this.fire();
    }

    public render(): void
    {
        super.render();

        /*switch (true) {
            case this.invulnerable:
                Stage.instance.context.fillStyle = 'white';
                break;

            case this.overheated:
                Stage.instance.context.fillStyle = 'yellow';
                break;

            default:
                Stage.instance.context.fillStyle = 'purple';
                break;
        }

        Stage.instance.context.beginPath();
        Stage.instance.context.arc(this.center.x, this.center.y, this.width / 2, 0, Math.PI * 2);
        Stage.instance.context.fill();*/

        const overheatBarWidth = 8;
        const overheatBarOffset = 2;
        const heatPercentage = this.currentHeat / this.maxHeat;

        const overheatBarX = this.x - overheatBarWidth - overheatBarOffset;
        const overheatCurrentHeight = this.height * heatPercentage;
        const overheatCurrentY = this.y + (this.height - overheatCurrentHeight);

        Stage.instance.context.fillStyle = '#630';
        Stage.instance.context.fillRect(overheatBarX, this.y, overheatBarWidth, this.height);

        Stage.instance.context.fillStyle = '#efa600';
        Stage.instance.context.fillRect(overheatBarX, overheatCurrentY, overheatBarWidth, overheatCurrentHeight);
    }

    public destroy(): void
    {
        super.destroy();

        Hero._instance = undefined;

        if (this.lifeTracker.parentNode) this.lifeTracker.parentNode.removeChild(this.lifeTracker);
    }

    public die(): void
    {
        if (this.dead || this.respawning || this.invulnerable) return;

        // this.deathAudio.play();

        this.stateMachine.change('dead');

        --this.lives;

        if (this.lives >= 0) {
            setTimeout(() => {
                this.center = Stage.instance.center;
                this.overheated = false;
                this.currentHeat = 0;
                this.stateMachine.change('respawning');
            }, this.respawnTime);
        } else {
            // PlayState.instance.bgm.fade = true;
            // PlayState.instance.bgm.pause();
            setTimeout(() => MasterMachine.instance.change('game-over'), this.respawnTime);
        }
    }

    public move(): void
    {
        const velocity = new Vector();

        if (Controller.instance.isDown('w')) velocity.y -= 1;
        if (Controller.instance.isDown('a')) velocity.x -= 1;
        if (Controller.instance.isDown('s')) velocity.y += 1;
        if (Controller.instance.isDown('d')) velocity.x += 1;

        velocity.normalize();

        if (!!velocity.x) this.x += (velocity.x * (this.speed * Time.deltaSeconds));
        if (!!velocity.y) this.y += (velocity.y * (this.speed * Time.deltaSeconds));

        this.moving = (velocity.x !== 0 || velocity.y !== 0);

        switch (true) {
            case velocity.x < 0:
                this.facing = 'left';
                break;

            case velocity.x > 0:
                this.facing = 'right';
                break;

            case velocity.y < 0:
                this.facing = 'up';
                break;

            case velocity.y > 0:
            default:
                this.facing = 'down';
                break;
        }

        // this.sprite.animationName = `${this.moving ? 'walk' : 'stand'}_${this.facing}`;

        if (this.top < Stage.instance.top) this.top = Stage.instance.top;
        if (this.left < Stage.instance.left) this.left = Stage.instance.left;
        if (this.bottom > Stage.instance.bottom) this.bottom = Stage.instance.bottom;
        if (this.right > Stage.instance.right) this.right = Stage.instance.right;
    }

    public fire(): void
    {
        const tryingToShoot = Controller.instance.mouseIsDown();

        if (tryingToShoot) {
            this.framesSinceLastFireAttempt = 0;
        } else {
            ++this.framesSinceLastFireAttempt;
        }

        if (!this.overheated && this.currentHeat >= this.maxHeat) {
            this.overheated = true;
            this.sprite.animationName = 'overheated';
        }

        if (this.framesSinceLastFireAttempt >= this.framesUntilCooling) {
            this.currentHeat -= this.coolingRate;
            if (this.currentHeat <= 0) {
                this.currentHeat = 0;
                if (this.overheated) {
                    this.overheated = false;
                    this.sprite.animationName = 'cooling';
                    this.framesSinceCooling = 0;
                }
            }
        }

        if (this.sprite.animationName === 'cooling') {
            ++this.framesSinceCooling;

            if (this.framesSinceCooling >= this.sprite.frameRate * this.sprite.animation.frames.length) {
                this.sprite.animationName = 'idle';
            }
        }

        if (this.canFire && !this.overheated) {
            if (tryingToShoot) {
                const mousePosition = Controller.instance.mousePosition();

                const projectile = new Projectile(mousePosition);
                projectile.center = this.center;

                this.currentHeat += projectile.heat;
                if (this.currentHeat >= this.maxHeat) this.currentHeat = this.maxHeat;

                if (this.hasSpread) {
                    const distanceX = mousePosition.x - this.center.x;
                    const distanceY = mousePosition.y - this.center.y;
                    const distance = Math.sqrt((distanceX * distanceX) + (distanceY * distanceY));
                    const distanceAngle = Math.atan2(distanceY, distanceX);

                    const leftX = this.center.x + distance * Math.cos(distanceAngle - 15 * (Math.PI / 180));
                    const leftY = this.center.y + distance * Math.sin(distanceAngle - 15 * (Math.PI / 180));
                    const leftProjectile = new Projectile(new Vector(leftX, leftY));
                    leftProjectile.center = this.center;

                    const rightX = this.center.x + distance * Math.cos(distanceAngle + 15 * (Math.PI / 180));
                    const rightY = this.center.y + distance * Math.sin(distanceAngle + 15 * (Math.PI / 180));
                    const rightProjectile = new Projectile(new Vector(rightX, rightY));
                    rightProjectile.center = this.center;
                }

                this.canFire = false;
                this.framesSinceLastFire = 0;
            }
        } else {
            ++this.framesSinceLastFire;
            if (this.framesSinceLastFire >= this.fireRate) {
                this.canFire = true;
                this.framesSinceLastFire = 0;
            }
        }
    }

    public static get instance(): Hero
    {
        return Hero._instance;
    }

    public get dead(): boolean
    {
        return this.stateMachine.state instanceof DeadState;
    }

    public get respawning(): boolean
    {
        return this.stateMachine.state instanceof RespawningState;
    }

    public get lives(): number
    {
        return this._lives;
    }

    public set lives(value: number)
    {
        this._lives = value;
        this.lifeTracker.innerText = `Lives: ${Math.max(this._lives, 0).toString()}`;
    }
}
