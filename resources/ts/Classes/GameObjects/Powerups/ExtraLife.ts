import Hero from 'Classes/GameObjects/Hero';
import Powerup from 'Classes/GameObjects/Powerups/Powerup';
import Stage from 'Classes/Stage';

export default class ExtraLife extends Powerup
{
    public up(): void
    {
        ++Hero.instance.lives;
    }

    public render(): void
    {
        Stage.instance.context.fillStyle = 'pink';
        Stage.instance.context.fillRect(this.x, this.y, this.width, this.height);
    }
}
