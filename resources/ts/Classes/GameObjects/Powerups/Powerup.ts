import GameObject from 'Classes/GameObjects/GameObject';
import Hero from 'Classes/GameObjects/Hero';

export default abstract class Powerup extends GameObject
{
    protected static _instances: Powerup[] = [];

    public width: number = 32;
    public height: number = 32;
    protected _duration: number = 0;
    protected _pickedUp: boolean = false;
    protected framesSincePickedUp: number = 0;

    constructor()
    {
        super();

        Powerup._instances.push(this);
    }

    public up(): void
    {
        //
    }

    public down(): void
    {
        //
    }

    public update(): void
    {
        if (this.pickedUp) {
            ++this.framesSincePickedUp;

            if (this.framesSincePickedUp > this.duration) {
                this.down();
                this.destroy();
            }
        } else if (this.colliding(Hero.instance)) {
            for (const powerup of Powerup.instances) {
                if (powerup === this) continue;
                if (powerup.constructor.name === this.constructor.name && powerup.pickedUp) {
                    powerup.framesSincePickedUp = 0;
                    return this.destroy();
                }
            }

            this._pickedUp = true;
            this.up();
        }
    }

    public destroy(): void
    {
        super.destroy();

        const index = Powerup._instances.indexOf(this);
        if (index >= 0) Powerup._instances.splice(index, 1);
    }

    public static get instances(): Powerup[]
    {
        return Powerup._instances;
    }

    public get duration(): number
    {
        return this._duration;
    }

    public get pickedUp(): boolean
    {
        return this._pickedUp;
    }
}
