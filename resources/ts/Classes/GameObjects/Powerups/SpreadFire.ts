import Hero from 'Classes/GameObjects/Hero';
import Powerup from 'Classes/GameObjects/Powerups/Powerup';
import Stage from 'Classes/Stage';

export default class SpreadFire extends Powerup
{
    protected _duration: number = 240;

    public up(): void
    {
        Hero.instance.hasSpread = true;
    }

    public down(): void
    {
        Hero.instance.hasSpread = false;
    }

    public render(): void
    {
        if (this.pickedUp) {
            const width = 160;
            const height = 16;
            const margin = 16;
            const filledPercentage = 1.0 - (this.framesSincePickedUp / this._duration);

            Stage.instance.context.fillStyle = '#6a0';
            Stage.instance.context.fillRect(margin, Stage.instance.bottom - height - margin, width, height);

            Stage.instance.context.fillStyle = 'yellow';
            Stage.instance.context.fillRect(margin, Stage.instance.bottom - height - margin, width * filledPercentage, height);
        } else {
            Stage.instance.context.fillStyle = 'yellow';
            Stage.instance.context.fillRect(this.x, this.y, this.width, this.height);
        }
    }
}

