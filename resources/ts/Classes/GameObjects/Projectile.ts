import Enemy from 'Classes/GameObjects/Enemy';
import GameObject from 'Classes/GameObjects/GameObject';
import Stage from 'Classes/Stage';
import Time from 'Classes/Abstracts/Time';
import Vector from 'Classes/Abstracts/Vector';
import VectorInterface from 'Interfaces/Vector';
import Wall from 'Classes/GameObjects/Wall';

export default class Projectile extends GameObject
{
    protected static _instances: Projectile[] = [];

    public width: number = 8;
    public height: number = 8;
    private _target: Vector;
    private speed: number = 640;
    private velocity: Vector;
    private damage: number = 1;
    private _heat: number = 0.1;

    constructor(target: Vector)
    {
        super();

        Projectile._instances.push(this);

        this.target = target;
    }

    public update(): void
    {
        this.x += this.velocity.x * this.speed * Time.deltaSeconds;
        this.y += this.velocity.y * this.speed * Time.deltaSeconds;

        if (this.offStage) return this.destroy();

        for (const wall of Wall.instances) {
            if (this.colliding(wall)) return this.destroy();
        }

        for (const enemy of Enemy.instances) {
            if (this.colliding(enemy)) {
                enemy.damage(this.damage);
                return this.destroy();
            }
        }
    }

    public render(): void
    {
        Stage.instance.context.fillStyle = 'yellow';
        Stage.instance.context.beginPath();
        Stage.instance.context.arc(this.center.x, this.center.y, this.width / 2, 0, Math.PI * 2);
        Stage.instance.context.fill();
    }

    public destroy(): void
    {
        super.destroy();

        const index = Projectile._instances.indexOf(this);
        if (index >= 0) Projectile._instances.splice(index, 1);
    }

    private calculateVelocity(): void
    {
        const distanceX = this.target.x - this.center.x;
        const distanceY = this.target.y - this.center.y;
        const angle = Math.atan2(distanceY, distanceX);

        this.velocity = new Vector(Math.cos(angle), Math.sin(angle));
        this.velocity.normalize();
    }

    public static get instances(): Projectile[]
    {
        return Projectile._instances;
    }

    public get center(): VectorInterface
    {
        return {
            x: this.x + (this.width / 2),
            y: this.y + (this.height / 2)
        };
    }

    public set center(value: VectorInterface)
    {
        this.x = value.x - (this.width / 2);
        this.y = value.y - (this.height / 2);
        this.calculateVelocity();
    }

    public get target(): Vector
    {
        return this._target;
    }

    public set target(value: Vector)
    {
        this._target = value;
        this.calculateVelocity();
    }

    public get heat(): number
    {
        return this._heat;
    }
}
