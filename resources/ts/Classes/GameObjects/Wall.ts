import GameObject from 'Classes/GameObjects/GameObject';
import Sprite from 'Classes/Abstracts/Sprite';
import SpriteConfigFactory from 'Sprites/WallSprite';
import Stage from 'Classes/Stage';
import Time from 'Classes/Abstracts/Time';
import Vector from 'Classes/Abstracts/Vector';

export default class Wall extends GameObject
{
    protected static _instances: Wall[] = [];

    public sprite: Sprite = new Sprite(this, SpriteConfigFactory());

    private velocity: Vector = new Vector();
    private speed: number = Math.randomFloatBetween(32, 64);
    private destroyable: boolean = false;
    private destroyTimer: number = Number(setTimeout(() => this.destroyable = true, 2500));

    constructor(x: number = 0, y: number = 0)
    {
        super(x, y);

        Wall._instances.push(this);

        this.width = this.sprite.width;
        this.height = this.sprite.height;

        this.collidable = true;
        this.immovable = true;

        // Choose a random edge of the stage to spawn the wall on.
        const spawnEdge = ['top', 'left', 'bottom', 'right'].random();

        // Choose a random spawn point and direction of movement based on spawn edge.
        const spawnPoint = new Vector();
        switch (spawnEdge) {
            case 'top':
                spawnPoint.x = Math.random() * Stage.instance.right;
                spawnPoint.y = -this.height;
                this.velocity.x = Math.randomFloatBetween(-0.5, 0.5);
                this.velocity.y = 1;
                break;

            case 'left':
                spawnPoint.x = -this.width;
                spawnPoint.y = Math.random() * Stage.instance.bottom;
                this.velocity.x = 1;
                this.velocity.y = Math.randomFloatBetween(-0.5, 0.5);
                break;

            case 'bottom':
                spawnPoint.x = Math.random() * Stage.instance.right;
                spawnPoint.y = Stage.instance.bottom + this.height;
                this.velocity.x = Math.randomFloatBetween(-0.5, 0.5);
                this.velocity.y = -1;
                break;

            case 'right':
            default:
                spawnPoint.x = Stage.instance.width + this.width;
                spawnPoint.y = Math.random() * Stage.instance.bottom;
                this.velocity.x = -1;
                this.velocity.y = Math.randomFloatBetween(-0.5, 0.5);
                break;
        }
        this.x = spawnPoint.x;
        this.y = spawnPoint.y;
    }

    public update(): void
    {
        this.x += (this.velocity.x * this.speed * Time.deltaSeconds);
        this.y += (this.velocity.y * this.speed * Time.deltaSeconds);

        for (const collidable of GameObject.collidables) {
            if (collidable instanceof Wall) continue;

            this.bumpAgainst(collidable, true);
        }

        if (this.destroyable && this.offStage) this.destroy();
    }

    public destroy(): void
    {
        super.destroy();

        const index = Wall._instances.indexOf(this);
        if (index >= 0) Wall._instances.splice(index, 1);
    }

    public static get instances(): Wall[]
    {
        return Wall._instances;
    }
}
