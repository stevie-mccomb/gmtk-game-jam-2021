import GameOverState from 'Classes/States/GameOverState';
import PlayState from 'Classes/States/PlayState';
import SplashState from 'Classes/States/SplashState';
import StateMachine from 'Classes/Machines/StateMachine';

export default class MasterMachine extends StateMachine
{
    private static _instance: MasterMachine;

    constructor()
    {
        super();

        MasterMachine._instance = this;

        this.register('splash', new SplashState());
        this.register('play', new PlayState());
        this.register('game-over', new GameOverState());

        this.change('splash');
    }

    public static get instance(): MasterMachine
    {
        return MasterMachine._instance;
    }
}
