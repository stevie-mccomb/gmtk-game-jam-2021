import State from 'Classes/States/State';

export default class StateMachine
{
    private static _instances: StateMachine[] = [];

    private states: State[] = [];
    private stateName: string;

    constructor()
    {
        StateMachine._instances.push(this);
    }

    public register(name: string, state: State): void
    {
        this.states[name] = state;
    }

    public change(name: string, data?: any): void
    {
        if (this.states[name] && this.stateName !== name) {
            if (this.state) this.state.exit();
            this.stateName = name;
            if (this.state) data ? this.state.enter(data) : this.state.enter();
        }
    }

    public update(): void
    {
        if (this.state) this.state.update();
    }

    public render(): void
    {
        if (this.state) this.state.render();
    }

    public static get instances(): StateMachine[]
    {
        return StateMachine._instances;
    }

    public get state(): State
    {
        return this.states[this.stateName];
    }
}
