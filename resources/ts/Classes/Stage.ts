import Game from 'Classes/Game';
import Vector from 'Interfaces/Vector';

export default class Stage
{
    private static _instance: Stage;
    private _element: HTMLCanvasElement;
    private _context: CanvasRenderingContext2D;
    private resizeBound: EventListener = this.resize.bind(this);

    constructor()
    {
        Stage._instance = this;

        this._element = document.createElement('canvas');
        this.element.className = 'stage';
        Game.instance.element.appendChild(this.element);

        this._context = this.element.getContext('2d');

        this.resize();
        window.addEventListener('resize', this.resizeBound);
    }

    private resize(): void
    {
        this.element.setAttribute('width', this.width.toString());
        this.element.setAttribute('height', this.height.toString());
    }

    public update(): void
    {
        //
    }

    public render(): void
    {
        this.context.clearRect(0, 0, this.width, this.height);
    }

    public get element(): HTMLCanvasElement
    {
        return this._element;
    }

    public get width(): number
    {
        return this.element.offsetWidth;
    }

    public get height(): number
    {
        return this.element.offsetHeight;
    }

    public get center(): Vector
    {
        return {
            x: (this.width / 2),
            y: (this.height / 2)
        };
    }

    public get top(): number
    {
        return 0;
    }

    public get left(): number
    {
        return 0;
    }

    public get bottom(): number
    {
        return this.height;
    }

    public get right(): number
    {
        return this.width;
    }

    public static get instance(): Stage
    {
        return Stage._instance;
    }

    public get context(): CanvasRenderingContext2D
    {
        return this._context;
    }
}
