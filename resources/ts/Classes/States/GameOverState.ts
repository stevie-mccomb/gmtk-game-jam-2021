import Game from 'Classes/Game';
import MasterMachine from 'Classes/Machines/MasterMachine';
import SFX from 'Classes/Audio/SFX';
import State from 'Classes/States/State';

export default class GameOverState extends State
{
    private element: HTMLDivElement = document.createElement('div');
    private title: HTMLHeadingElement = document.createElement('h2');
    private button: HTMLButtonElement = document.createElement('button');

    private onButtonClickedBound: EventListener = this.onButtonClicked.bind(this);

    // private bgm: SFX = new SFX('/audio/bgm/game-over.mp3');

    constructor()
    {
        super();

        this.element.className = 'game-over-container';
        this.title.innerText = 'Game Over!';
        this.button.innerText = 'Start New Game';

        this.element.appendChild(this.title);
        this.element.appendChild(this.button);

        this.button.addEventListener('click', this.onButtonClickedBound);
    }

    public enter(): void
    {
        Game.instance.element.appendChild(this.element);

        // this.bgm.currentTime = 0.0;
        // this.bgm.fade = true;
        // this.bgm.play();
    }

    public exit(): void
    {
        // this.bgm.pause();

        if (this.element.parentNode) this.element.parentNode.removeChild(this.element);
    }

    private onButtonClicked(e: MouseEvent): void
    {
        MasterMachine.instance.change('play');
    }
}
