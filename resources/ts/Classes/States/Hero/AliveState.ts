import Hero from 'Classes/GameObjects/Hero';
import Stage from 'Classes/Stage';
import State from 'Classes/States/State';

export default class AliveState extends State
{
    public enter(): void
    {
        Hero.instance.sprite.animationName = 'idle';
    }
}
