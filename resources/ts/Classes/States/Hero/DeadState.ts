import Hero from 'Classes/GameObjects/Hero';
import Powerup from 'Classes/GameObjects/Powerups/Powerup';
import State from 'Classes/States/State';

export default class DeadState extends State
{
    public enter(): void
    {
        Hero.instance.sprite.animationName = 'death';

        for (let i = Powerup.instances.length - 1; i >= 0; --i) {
            if (Powerup.instances[i].pickedUp) Powerup.instances[i].down();
            Powerup.instances[i].destroy();
        }
    }
}
