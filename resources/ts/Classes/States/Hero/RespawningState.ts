import Hero from 'Classes/GameObjects/Hero';
import Stage from 'Classes/Stage';
import State from 'Classes/States/State';

export default class RespawningState extends State
{
    private respawnDelay: number = 1500;
    private respawnTimer: number = 0;

    public enter(): void
    {
        Hero.instance.sprite.animationName = 'respawning';

        if (this.respawnTimer) clearTimeout(this.respawnTimer);
        this.respawnTimer = Number(setTimeout(() => Hero.instance.stateMachine.change('alive'), this.respawnDelay));
    }
}
