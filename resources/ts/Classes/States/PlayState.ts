import Enemy from 'Classes/GameObjects/Enemy';
import EnemySpawner from 'Classes/Abstracts/EnemySpawner';
import Game from 'Classes/Game';
import GameObject from 'Classes/GameObjects/GameObject';
import Hero from 'Classes/GameObjects/Hero';
import PowerupSpawner from 'Classes/Abstracts/PowerupSpawner';
import SFX from 'Classes/Audio/SFX';
import Stage from 'Classes/Stage';
import State from 'Classes/States/State';
import Time from 'Classes/Abstracts/Time';
import Wall from 'Classes/GameObjects/Wall';
import WallSpawner from 'Classes/Abstracts/WallSpawner';

export default class PlayState extends State
{
    private static _instance: PlayState;

    private _score: number = 0;
    private scoreElement: HTMLDivElement = document.createElement('div');
    private scoreLastUpdated: number = 0;

    // public bgm: SFX = new SFX('/audio/bgm/winds-of-stories.mp3');

    constructor()
    {
        super();

        PlayState._instance = this;

        this.scoreElement.className = 'score';
        this.score = 0;
    }

    public enter(): void
    {
        Game.instance.element.appendChild(this.scoreElement);

        new Hero();

        new EnemySpawner();
        new PowerupSpawner();
        new WallSpawner();

        // this.bgm.currentTime = 0.0;
        // this.bgm.fade = true;
        // this.bgm.play();
    }

    public update(): void
    {
        if (!Hero.instance.dead && Time.now > (this.scoreLastUpdated + 1000)) {
            ++this.score;
            this.scoreLastUpdated = Time.now;
        }

        for (const gameObject of GameObject.instances) {
            gameObject.update();
        }
    }

    public render(): void
    {
        for (const gameObject of GameObject.instances) {
            gameObject.render();
        }
    }

    public exit(): void
    {
        Hero.instance.destroy();

        for (let i = Wall.instances.length - 1; i >= 0; --i) {
            Wall.instances[i].destroy();
        }

        for (let i = Enemy.instances.length - 1; i >= 0; --i) {
            Enemy.instances[i].destroy();
        }

        EnemySpawner.instance.destroy();

        if (this.scoreElement.parentNode) this.scoreElement.parentNode.removeChild(this.scoreElement);
        this.score = 0;

        // this.bgm.pause();
    }

    public static get instance(): PlayState
    {
        return PlayState._instance;
    }

    public get score(): number
    {
        return this._score;
    }

    public set score(value: number)
    {
        this._score = value;
        this.scoreElement.innerText = `Score: ${this._score}`;
    }
}
