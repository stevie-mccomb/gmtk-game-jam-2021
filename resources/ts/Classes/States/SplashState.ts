import Game from 'Classes/Game';
import MasterMachine from 'Classes/Machines/MasterMachine';
import State from 'Classes/States/State';

export default class SplashState extends State
{
    private static _instance: SplashState;

    private container: HTMLDivElement = document.createElement('div');
    private logo: HTMLHeadingElement = document.createElement('h1');
    private playButton: HTMLButtonElement = document.createElement('button');

    private onPlayButtonClickedBound: EventListener = this.onPlayButtonClicked.bind(this);

    constructor()
    {
        super();

        SplashState._instance = this;

        this.container.className = 'splash-screen';
        this.logo.className = 'logo';
        this.logo.innerText = 'Game Jam';
        this.playButton.className = 'play-button';
        this.playButton.setAttribute('type', 'button');
        this.playButton.innerText = 'Play';

        this.playButton.addEventListener('click', this.onPlayButtonClickedBound);

        this.container.appendChild(this.logo);
        this.container.appendChild(this.playButton);
    }

    public enter(): void
    {
        Game.instance.element.appendChild(this.container);
    }

    public exit(): void
    {
        this.container.parentNode.removeChild(this.container);
    }

    private onPlayButtonClicked(e: MouseEvent): void
    {
        MasterMachine.instance.change('play');
    }

    public static get instance(): SplashState
    {
        return SplashState._instance;
    }
}
