export default class State
{
    private static _instances: State[] = [];

    constructor()
    {
        State._instances.push(this);
    }

    public enter(data?: any): void
    {
        //
    }

    public exit(): void
    {
        //
    }

    public update(): void
    {
        //
    }

    public render(): void
    {
        //
    }

    public get instances(): State[]
    {
        return State._instances;
    }
}
