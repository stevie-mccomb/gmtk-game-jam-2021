declare global {
    interface Math {
        randomFloatBetween(min: number, max: number): number;
        randomIntBetween(min: number, max: number): number;
    }
}

Math.randomFloatBetween = function(min: number, max: number): number
{
    return (Math.random() * (max - min)) + min;
};

Math.randomIntBetween = function(min: number, max: number): number
{
    return Math.floor(Math.randomFloatBetween(min, max));
};

export default Math;
