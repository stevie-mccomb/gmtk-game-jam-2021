export default interface ControllerListener
{
    event: string;
    callback: EventListener;
}
