import Vector from 'Interfaces/Vector';

export default interface MouseData
{
    buttonDown: boolean;
    position: Vector;
};
