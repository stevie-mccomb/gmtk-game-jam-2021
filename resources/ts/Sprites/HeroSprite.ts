import SpriteConfig from 'Interfaces/SpriteConfig';

export default {
    src: '/img/sprites/hero.png',

    frame_rate: 16,

    width: 32,
    height: 32,

    default_animation: 'idle',

    animations: {
        idle: {
            frames: [
                {
                    x: 0,
                    y: 0
                },

                {
                    x: 32,
                    y: 0
                },

                {
                    x: 64,
                    y: 0
                },

                {
                    x: 32,
                    y: 0
                },

                {
                    x: 0,
                    y: 0
                }
            ]
        },

        invulnerable: {
            frame_rate: 4,
            frames: [
                {
                    x: 0,
                    y: 32
                },

                {
                    x: 32,
                    y: 32
                },

                {
                    x: 64,
                    y: 32
                },

                {
                    x: 32,
                    y: 32
                }
            ]
        },

        death: {
            loop: false,
            frames: [
                {
                    x: 0,
                    y: 64
                },

                {
                    x: 32,
                    y: 64
                },

                {
                    x: 64,
                    y: 64
                }
            ]
        },

        respawning: {
            loop: false,
            frames: [
                {
                    x: 64,
                    y: 64
                },

                {
                    x: 32,
                    y: 64
                },

                {
                    x: 0,
                    y: 64
                },

                {
                    x: 0,
                    y: 0
                }
            ]
        },

        overheated: {
            loop: false,
            frames: [
                {
                    x: 0,
                    y: 96
                },

                {
                    x: 32,
                    y: 96
                },

                {
                    x: 64,
                    y: 96
                }
            ]
        },

        cooling: {
            loop: false,
            frames: [
                {
                    x: 64,
                    y: 96
                },

                {
                    x: 32,
                    y: 96
                },

                {
                    x: 0,
                    y: 96
                },

                {
                    x: 0,
                    y: 0
                }
            ]
        }
    }
};
