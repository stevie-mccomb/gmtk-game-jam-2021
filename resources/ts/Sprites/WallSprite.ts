import SpriteConfig from 'Interfaces/SpriteConfig';

const walls = [
    {
        src: '/img/sprites/wall_01.png',
        width: 269,
        height: 218
    },

    {
        src: '/img/sprites/wall_02.png',
        width: 133,
        height: 137
    },

    {
        src: '/img/sprites/wall_03.png',
        width: 190,
        height: 115
    },

    {
        src: '/img/sprites/wall_04.png',
        width: 215,
        height: 230
    }
];

const spriteConfigFactory = function(): SpriteConfig
{
    const spriteConfig = {
        src: walls[0].src,

        frame_rate: 16,

        width: walls[0].width,
        height: walls[0].height,

        default_animation: 'idle',

        animations: {
            idle: {
                frames: [
                    {
                        x: 0,
                        y: 0
                    }
                ]
            }
        }
    };

    const wall = walls.random();
    spriteConfig.src = wall.src;
    spriteConfig.width = wall.width;
    spriteConfig.height = wall.height;

    return spriteConfig;
};

export default spriteConfigFactory;
