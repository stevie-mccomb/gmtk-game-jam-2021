import './Extensions/Math.ts';
import './Prototypes/Array.ts';
import './Prototypes/Number.ts';
import Game from 'Classes/Game';

document.addEventListener('DOMContentLoaded', e => new Game());
