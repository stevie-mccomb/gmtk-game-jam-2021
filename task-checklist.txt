Projectiles
--------
    Summary
    --------
        The hero can shoot projectiles at the bad guys to hurt them. The more projectiles the hero fires, the hotter their gun gets. The player needs to manage heat to prevent overheating. When enemies are hit by projectiles, they are hurt. If they are hurt enough, they die.

    Tasks
    --------
        0. Making projectiles
            X - 0.0. Listen for user input (mouse input, twin-stick shooter).
            X - 0.1. Create new projectile instance.
            X - 0.2. Position the projectile in the middle of the hero.
            X - 0.3. Travel towards the point where the click occurred at a set velocity.
            X - 0.4. If projectiles leave the screen, they need to destroy themselves.
            X - 0.5. Limit to how many can be generated in a given time period (rate of fire).
        1. Projectile collisions
            X - 1.0. Projectile should collide with enemies and walls.
            X - 1.1. If collides with wall, projectile should destroy itself.
            X - 1.2. If collides with enemy, projectile should destroy itself and enemy should lose health at a rate determined by the projectile.
            X - 1.3. If enemy health is below zero, enemy should destroy itself.
            X - 1.4. Enemies should display a health bar to gauge their current health during gameplay.
        2. Overheating
            X - 2.0. Hero should have variables for tracking current heat and max heat.
            X - 2.1. Heat increases whenever a projectile is created.
            X - 2.2. Heat decreases incrementally over time (after a delay of not firing).
            X - 2.3. If current heat exceeds max heat, prevent the hero from firing for a set time period, then should reset.
            X - 2.4. These variables should be represented by a UI meter.
        3. Powerups
            X - 3.0. Create a new "Powerup" GameObject.
            X - 3.1. Four powerup variants.
                X - 3.1.0. Increase player lives.
                X - 3.1.1. Rapid-fire ammunition.
                X - 3.1.2. Invulnerability.
                X - 3.1.3. Spread ammunition.
            X - 3.2. Random powerups spawn on a timer.
            X - 3.3. Player needs to collide with the powerups to pick them up.
            X - 3.4. Powerup destroys itself when it collides with the hero.
            X - 3.5. Powerup needs to cause some sort of change/action.
            X - 3.6. Powerups wear off after a set time (unique to each powerup).
            X - 3.7. Show a UI timer for each active powerup, indicating how long until it wears off.
            X - 3.8. Picking up a powerup we already have active resets the timer.
        4. Lines between enemies
            _ - 4.0. //

------------------------

Walls
--------
    0. Randomize wall size/shape by choosing from set prefabs.
        X - 0.0. Make a spawner/spawn-manager.
        X - 0.1. Make the prefabs.
        X - 0.2. Randomly choose a prefab.
    1. Choose a random spawn point (outside of the stage).
        X - 1.0. Randomly choose one edge of the stage.
        X - 1.1. Randomly choose a point on that edge.
    2. Choose a random direction to move in (towards the stage, not away from it).
        X - 2.0. Choose a random velocity that's the opposite of our spawn edge.
        X - 2.1. Choose a random velocity on the other axis that's within a range (-72/72 degrees).
    3. Choose a random speed for it to move at.
        X - 3.0. Generate a random number between a minimum and maximum speed.
    4. Destroy the wall once it leaves the stage.
        X - 4.0. Use a collision-detection-style comparison to determine if the wall is completely out of view.
        X - 4.1. Destroy it if it is.
