const mix = require('laravel-mix');
const path = require('path');

mix.disableSuccessNotifications();

mix.webpackConfig({
    resolve: {
        modules: [
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, 'resources/ts')
        ]
    }
});

mix.options({
    processCssUrls: false,
    publicPath: 'public'
});

mix.ts('resources/ts/app.ts', 'public/js');
mix.sass('resources/sass/app.scss', 'public/css');
